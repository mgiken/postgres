FROM postgres:11.5

ENV PLV8_VERSION=2.3.9

RUN BUILD_DEPS=" \
    ca-certificates \
    curl \
    g++ \
    git-core \
    libc++abi-dev \
    make \
    pkg-config \
    postgresql-server-dev-${PG_MAJOR} \
    python" \
 && apt-get update \
 && apt-get install -y --no-install-recommends libc++-dev ${BUILD_DEPS} \
 && curl -sSL "https://github.com/plv8/plv8/archive/v${PLV8_VERSION}.tar.gz" | tar zx -C /tmp \
 && cd /tmp/plv8-${PLV8_VERSION} \
 && make \
 && make install \
 && strip /usr/lib/postgresql/${PG_MAJOR}/lib/plv8-${PLV8_VERSION}.so \
 && apt-get remove -y ${BUILD_DEPS} \
 && apt-get autoremove -y \
 && apt-get clean \
 && rm -rf /tmp/plv8-${PLV8_VERSION} /var/lib/apt/lists/*
